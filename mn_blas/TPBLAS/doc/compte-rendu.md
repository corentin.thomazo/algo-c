% MN  Compte rendu de TP 2

# **Compte-rendu de TP 2 BLAS**

## Participants 

* BONFILS Antoine
* THOMAZO Corentin

## **Calculs sur les nombres complexes**

### 1. complexe.c

Nous avons retranscrit les opérations sur les complexes dans notre fichier ```complexe.c```. 

Précision pour la division de complexes: 

Nous avons implémenter les fonctions ```conj_complexe_float``` et ```conj_complexe_double``` pour la création de complexe conjugué.

### 2. test_complexe.c

Pour tester les fonctions implémentées dans la questions précédentes, nous avons d'abord tester si leurs résultats sont juste (de la ligne 31 à 38), puis observer les performances (de la ligne 59 à 87).

### 3. complexe2.h

Nous avons importé les fonctions implémentées dans ```complexe.c``` dans ```complexe2.h```, à la différence que ces dernières sont maintenant ```inlinées```.

### 4. test_complexe2

Après avoir lancer l'exécution du programme ```test_complexe2```, nous avons remarquer que les performances en GFLOP/s sont doublées, par extension avec une durée de moitié, en comparaison avec le test précédent.

```
./test_complexe
residu_micro = 1.000000e-06
c1.r 4.000000 c1.i 8.000000
c1.r -9.000000 c1.i 12.000000
c1.r 0.333333 c1.i 0.000000
Pour la fonction add
apres boucle cd1.real 12810.000000 cd1.imaginary 16391.000000 duree 0.000004 
calcul complexe  1024 operations duree 0.0000040 seconde  Performance 0.256 GFLOP/s


Pour la fonction mult
apres boucle cd1.real -nan cd1.imaginary -nan duree 0.000007 
calcul complexe  1024 operations duree 0.0000070 seconde  Performance 0.146 GFLOP/s
Pour la fonction div
apres boucle cd1.real 0.000000 cd1.imaginary -0.000000 duree 0.000035 
calcul complexe  1024 operations duree 0.0000350 seconde  Performance 0.029 GFLOP/s

```


```
./test_complexe2
residu_micro = 0.000000e+00
c1.r 4.000000 c1.i 8.000000
apres boucle cd1.real 12810.000000 cd1.imaginary 16391.000000 0.000002 
calcul complexe  1024 operations duree 0.0000020 seconde  Performance 0.512 GFLOP/s

```

### 5. test_complexe3.c


### 6. Comparaison de test_complexe et test_complexe3



## **Fonctions BLAS1**

### 7. Les fonctions de ```swap```


### 8. Les fonctions de ```copy```


### 9. Les fonctions de ```dot```


### 10. Les fonctions de ```axpy```

Pour les fonctions concernantes les complexes, nous devons utilisé les fonctions implémentées dans `complexe.c`.

### 11. Les fonctions de ```asum```


### 12. Les fonctions de ```iamin```


### 13. Les fonctions de ```iamax```


### 14. Les fonctions de ```nrm2```


## **Fonctions BLAS2**
### 15. Les fonctions de ```gemv```

## **Fonctions BLAS3**
### 16. Les fonctions de ```gemm```


## **OpenMP**

### 17. Les fonctions de ```copy```, ```dot```, ```axpy``` avec OpenMP


### 18. Les fonctions de ```gemv``` avec OpenMP


### 19. Les fonctions de ```gemm``` avec OpenMP


### 20. 
|                 	| Sans OPENMP                   	| avec OPENMP                   	|
|               	|---------------------	            |---------------------	            |
|                 	| BLAS1 	| BLAS2 	| BLAS3 	| BLAS1 	| BLAS2 	| BLAS3 	|
|-----------------	|-------	|-------	|-------	|-------	|-------	|-------	|
| float           	|       	|       	|       	|       	|       	|       	|
| double          	|       	|       	|       	|       	|       	|       	|
| complexe float  	|       	|       	|       	|       	|       	|       	|
| complexe double 	|       	|       	|       	|       	|       	|       	|