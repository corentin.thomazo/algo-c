#include "mnblas.h"
#include "complexe2.h"

#define numberthread 8

/***   Computes a vector-scalar product and adds the result to a vector.   ***/
/*      y := a*x + y    */

/*  float  */
void mnblas_saxpyOpenMP(const int N, const float alpha, const float *X,
                 const int incX, float *Y, const int incY){
    int n_per_thread=N/numberthread;
    omp_set_num_threads(numberthread);
    register unsigned int i = 0 ;
    #pragma omp parallel for shared(X, Y, alpha) private(i) schedule(static, n_per_thread)
    
    for( i = 0; i < N; i ++){
        Y[i*incY] = alpha * X[i*incX] + Y[i*incY];
    }
}

/*  double  */
void mnblas_daxpyOpenMP(const int N, const double alpha, const double *X,
                 const int incX, double *Y, const int incY){
    int n_per_thread=N/numberthread;
    omp_set_num_threads(numberthread);
    register unsigned int i = 0 ;
    #pragma omp parallel for shared(X, Y, alpha) private(i) schedule(static, n_per_thread)

    for( i = 0; i <N; i += incX){
        Y[i*incY] = alpha * X[i*incX] + Y[i*incY];
    }
}

/*  complex  */
void mnblas_caxpyOpenMP(const int N, const void *alpha, const void *X,
                 const int incX, void *Y, const int incY){
    register unsigned int i = 0 ;
    const complexe_float_t *comp_X = X;
    complexe_float_t *comp_Y = Y;
    const complexe_float_t *a=alpha;
    complexe_float_t tmp_mult;
	int n_per_thread=N/numberthread;
    omp_set_num_threads(numberthread);
    #pragma omp parallel for shared(X, Y, alpha) private(i) schedule(static, n_per_thread)
    for( i = 0; i < N; i ++){
        tmp_mult = mult_complexe_float(*a,comp_X[i*incX]);
        comp_Y[i*incY] = add_complexe_float(tmp_mult ,comp_Y[i*incY]);

    }
}

/*  double complex  */
void mnblas_zaxpyOpenMP(const int N, const void *alpha, const void *X,
                 const int incX, void *Y, const int incY){
    register unsigned int i = 0 ;
    const complexe_double_t *comp_X = X;
    complexe_double_t *comp_Y = Y;
    const complexe_double_t *a=alpha;
    complexe_double_t tmp_mult;
    int n_per_thread=N/numberthread;
    omp_set_num_threads(numberthread);
    #pragma omp parallel for shared(X, Y, alpha) private(i) schedule(static, n_per_thread)
    //#pragma omp parallel for shared(comp_Y,a,comp_X) //reduction(+:j)     // private(comp_Y)
    for( i = 0; i < N; i ++){
        tmp_mult = mult_complexe_double(*a,comp_X[i*incX]);
        comp_Y[i*incY] = add_complexe_double(tmp_mult ,comp_Y[i*incY]);
    }
}