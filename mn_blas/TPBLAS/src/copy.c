#include "mnblas.h"
#include "complexe2.h"

/***   Copies vector to another vector.   ***/

/*  float  */
void mncblas_scopy(const int N, const float *X, const int incX, 
                 float *Y, const int incY)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;

  for (; ((i < N) && (j < N)) ; i += incX, j += incY)
    {
      Y [j] = X [i] ;
    }

  return ;
}

/*  double  */
void mncblas_dcopy(const int N, const double *X, const int incX, 
                 double *Y, const int incY)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;

  for (; ((i < N) && (j < N)) ; i += incX, j += incY)
    {
      Y [j] = X [i] ;
    }

  return ;

}

/*  float complex  */
void mncblas_ccopy(const int N, const void *X, const int incX, 
		                    void *Y, const int incY)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;

  const complexe_float_t *comp_X = X;
  complexe_float_t *comp_Y = Y;

  for (; ((i < N) && (j < N)) ; i += incX, j += incY)
    {
      comp_Y[j].real = comp_X[i].real ;
      comp_Y[j].imaginary = comp_X[i].imaginary ;
    }
    
  return ;
}

/*  double complex  */
void mncblas_zcopy(const int N, const void *X, const int incX, 
		                    void *Y, const int incY)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;

  const complexe_double_t *comp_X = X;
  complexe_double_t *comp_Y = Y;

  for (; ((i < N) && (j < N)) ; i += incX, j += incY)
    {
      comp_Y[j].real = comp_X[i].real ;
      comp_Y[j].imaginary = comp_X[i].imaginary ;
    }

  return ;

}

