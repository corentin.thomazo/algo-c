#include "mnblas.h"
#include "complexe2.h"
#include <stdio.h>

void mncblas_sgemv(const MNCBLAS_LAYOUT layout,
                 const MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const float alpha, const float *A, const int lda,
                 const float *X, const int incX, const float beta,
                 float *Y, const int incY){
                     /* On suppose que layout est toujours en row major
                     * 
                     */
    float summ;
    register unsigned int i = 0 ;
    register unsigned int j = 0 ;
    for ( i = 0 ; i < N ; i++ ){
        summ = 0;
        for ( j = 0 ; j < M ; j++ ){
            summ +=alpha*A[j+i*M]*X[j];
        }
        Y[i] = summ+beta*Y[i];
    }
}

void mncblas_dgemv(MNCBLAS_LAYOUT layout,
                 MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const double alpha, const double *A, const int lda,
                 const double *X, const int incX, const double beta,
                 double *Y, const int incY){
    register unsigned int i = 0 ;
    register unsigned int j = 0 ;
    double summ;
    
    for ( i = 0 ; i < N ; i++ ){
        summ = 0;
        for ( j = 0 ; j < M ; j++ ){
            summ +=alpha*A[j+i*M]*X[j];
        }
        Y[i] = summ+beta*Y[i];
    }     
}

void mncblas_cgemv(MNCBLAS_LAYOUT layout,
                 MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const void *alpha, const void *A, const int lda,
                 const void *X, const int incX, const void *beta,
                 void *Y, const int incY){
    register unsigned int i = 0 ;
    register unsigned int j = 0 ;
    complexe_float_t summ;
    
    
    const complexe_float_t* M1=A;
    const complexe_float_t* C1=X;                
    const complexe_float_t* ALPHA=alpha;
    const complexe_float_t* BETA=beta;    
    complexe_float_t* C2=Y;
    
    for ( i = 0 ; i < N ; i++ ){
        summ.real= 0;
        summ.imaginary=0;
        for ( j = 0 ; j < M ; j++ ){
            summ =add_complexe_float(mult_complexe_float(mult_complexe_float(*ALPHA,M1[j+i*M]),C1[j]),summ);
        }
         C2[i]= add_complexe_float( summ,mult_complexe_float(*BETA,C2[i]));
    }
}

void mncblas_zgemv(MNCBLAS_LAYOUT layout,
                 MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const void *alpha, const void *A, const int lda,
                 const void *X, const int incX, const void *beta,
                 void *Y, const int incY){
    register unsigned int i = 0 ;
    register unsigned int j = 0 ;
    complexe_double_t summ;
    
    
    const complexe_double_t* M1=A;
    const complexe_double_t* C1=X;                
    const complexe_double_t* ALPHA=alpha;
    const complexe_double_t* BETA=beta;    
    complexe_double_t* C2=Y;
    for ( i = 0 ; i < N ; i++ ){
        summ.real= 0;
        summ.imaginary=0;
        for ( j = 0 ; j < M ; j++ ){
            summ =add_complexe_double(mult_complexe_double(mult_complexe_double(*ALPHA,M1[j+i*M]),C1[j]),summ);
        }
        C2[i]= add_complexe_double( summ,mult_complexe_double(*BETA,C2[i]));
    }
}