#include "mnblas.h"
#include "complexe2.h"

/***    Finds the index of the element with maximum absolute value.     ***/

/*  entier  */
CBLAS_INDEX mnblas_isamax(const int N, const float  *X, const int incX){
    CBLAS_INDEX index = -1;
    float max_value = 0.0;
    register unsigned int i = 0 ;
    
    for ( i = 0; i < N; i += incX)
    {
        if(max_value < X[i]){
            max_value = X[i];
            index = i;
        }
    }
    return index;    
}

/*  double  */
CBLAS_INDEX mnblas_idamax(const int N, const double *X, const int incX){
    CBLAS_INDEX index = -1;
    double max_value = 0.0;
    register unsigned int i = 0 ;

    for ( i = 0; i < N; i += incX)
    {
        if(max_value < X[i]){
            max_value = X[i];
            index = i;
        }
    }
    return index;    
}

/*  complex  */
CBLAS_INDEX mnblas_icamax(const int N, const void   *X, const int incX){
    CBLAS_INDEX index = -1;
    float max_value = 0.0;
    register unsigned int i = 0 ;

    const complexe_float_t *comp_X = X;
    
    for ( i = 0; i < N; i += incX)
    {
        float sum = comp_X[i].real + comp_X[i].imaginary;
        if(max_value < sum){
            max_value = sum;
            index = i;
        }
    }
    return index;    
}

/*  double complex  */
CBLAS_INDEX mnblas_izamax(const int N, const void   *X, const int incX){
    CBLAS_INDEX index = -1;
    double max_value = 0.0;
    register unsigned int i = 0 ;

    const complexe_float_t *comp_X = X;

    for ( i = 0; i < N; i += incX)
    {
        double sum = comp_X[i].real + comp_X[i].imaginary;
        if(max_value < sum){
            max_value = sum;
            index = i;
        }
    }
    return index;     
}