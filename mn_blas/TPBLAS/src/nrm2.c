#include "mnblas.h"
#include "complexe.h"
#include "math.h"

float  mnblas_snrm2(const int N, const float *X, const int incX){
    //return sqrt(mncblas_sdot(N,*X,incX,*X,incX));
    float res=0;

    for (int i = 0; i < N; i += incX){
        res+=X[i]*X[i];
    }
    return sqrt(res);
}

double mnblas_dnrm2(const int N, const double *X, const int incX){
    double res=0;

    for (int i = 0; i < N; i += incX){
        res+=X[i]*X[i];
    }
    return sqrt(res);
}

float  mnblas_scnrm2(const int N, const void *X, const int incX){
    float res=0;
    register unsigned int i;
  
    const complexe_float_t *comp_X = X;

    for (i = 0; i < N; i += incX){
        res+= sqrt(comp_X[i].real*comp_X[i].real + comp_X[i].imaginary*comp_X[i].imaginary);
    }
    return sqrt(res);
}

double mnblas_dznrm2(const int N, const void *X, const int incX){
    double res=0;
    register unsigned int i;
  
    const complexe_double_t *comp_X = X;

    for (i = 0; i < N; i += incX){
        res+= sqrt(comp_X[i].real*comp_X[i].real + comp_X[i].imaginary*comp_X[i].imaginary);
    }
    return sqrt(res);
}