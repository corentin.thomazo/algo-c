#include "mnblas.h"
#include "complexe2.h"

/***    Finds the index of the element with minimum absolute value.     ***/

/*  entier  */
CBLAS_INDEX mnblas_isamin(const int N, const float  *X, const int incX){
    CBLAS_INDEX index = -1;
    if(N == 0){ //si le vecteur est vide, on retourne -1.
        return index;
    }
    float min_value = X[0];
    register unsigned int i = 0 ;

    for ( i = 1; i < N; i += incX)
    {
        if(min_value > X[i]){
            min_value = X[i];
            index = i;
        }
    }
    return index;    
}

/*  double  */
CBLAS_INDEX mnblas_idamin(const int N, const double *X, const int incX){
    CBLAS_INDEX index = -1;
    if(N == 0){ //si le vecteur est vide, on retourne -1.
        return index;
    }
    double min_value = X[0];
    register unsigned int i = 0 ;
    for ( i = 1; i < N; i += incX)
    {
        if(min_value > X[i]){
            min_value = X[i];
            index = i;
        }
    }
    return index;    
}

/*  complex  */
CBLAS_INDEX mnblas_icamin(const int N, const void   *X, const int incX){
    CBLAS_INDEX index = -1;
    if(N == 0){ //si le vecteur est vide, on retourne -1.
        return index;
    }

    const complexe_float_t *comp_X = X;

    float min_value = comp_X[0].real + comp_X[0].imaginary;
    register unsigned int i = 0 ;
    for ( i = 1; i < N; i += incX)
    {
        float sum = comp_X[i].real + comp_X[i].imaginary;

        if(min_value > sum){
            min_value = sum;
            index = i;
        }
    }
    return index;
}

/*  double complex  */
CBLAS_INDEX mnblas_izamin(const int N, const void   *X, const int incX){
    CBLAS_INDEX index = -1;
    if(N == 0){ //si le vecteur est vide, on retourne -1.
        return index;
    }

    const complexe_float_t *comp_X = X;

    double min_value = comp_X[0].real + comp_X[0].imaginary;
    register unsigned int i = 0 ;
    for ( i = 1; i < N; i += incX)
    {
        double sum = comp_X[i].real + comp_X[i].imaginary;

        if(min_value > sum){
            min_value = sum;
            index = i;
        }
    }
    return index;    
}