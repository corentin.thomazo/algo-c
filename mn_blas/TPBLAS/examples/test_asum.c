#include <stdio.h>

#include "mnblas.h"
#include "complexe.h"

#include "flop.h"

#define VECSIZE    65536

#define NB_FOIS    10

typedef float vfloat [VECSIZE] ;
typedef double vdouble [VECSIZE] ;
typedef complexe_float_t vcomplexe [VECSIZE] ;
typedef complexe_double_t vcomplexedouble [VECSIZE] ;

vfloat vec1;
vdouble vec1d;
vcomplexe vec3;
vcomplexedouble vec3d;

void vector_init (vfloat V, float x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    V [i] = x ;

  return ;
}

void vector_init_double (vdouble V, double x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    V [i] = x ;

  return ;
}

void vector_complexe_init (vcomplexe V, float a, float b)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++){
    V[i].real = a ;
    V[i].imaginary = b ;
  }
  return ;
}

void vector_complexe_init_double (vcomplexedouble V, double a, double b)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++){
    V[i].real = a ;
    V[i].imaginary = b ;
  }
  return ;
}


void vector_print (vfloat V)
{
  register unsigned int i = 0;

  //for (i = 0; i < VECSIZE; i++)
    printf ("%f ", V[i]) ;
  printf ("\n") ;
  
  return ;
}

void vector_complexe_print (vcomplexe V){
  register unsigned int i = 0;

  //for (i = 0; i < VECSIZE; i++){
    printf ("%f ", V[i].real) ;
    printf ("%f ", V[i].imaginary) ;
  //}
  printf ("\n") ;
  
  return ;
}

int main (int argc, char **argv)
{
 struct timeval start, end ;
 //unsigned long long int start_tsc, end_tsc ;
 
 float res ;
 int i ;
 printf ("==========================================================\n") ;
 printf (" |         TEST FONCTIONNALITE -  FLOAT         |\n") ;
 printf ("==========================================================\n") ;
  
  vector_init (vec1, 1.0);
  printf("Vec1 = ");
  vector_print(vec1);
  printf("*** Utilisation de la fonction mnblas_sasum...\n");
    res = mnblas_sasum (VECSIZE, vec1, 1);
  printf("Vec1 = %f\n", *vec1);

 printf ("res = %f\n", res) ;
 printf ("==========================================================\n") ;
 printf (" |     TEST FONCTIONNALITE - COMPLEXE DOUBLE / FLOAT     |\n") ;
 printf ("==========================================================\n") ;
  
  vector_complexe_init (vec3, 1.0, 2.0);
  printf("Vec3 = ");
  vector_complexe_print(vec3);
  printf("*** Utilisation de la fonction mnblas_sasum...\n");
  res = mnblas_scasum (VECSIZE, vec3, 1);
  printf("Vec3 = %f %f\n", vec3->real, vec3->imaginary);
 printf ("res = %f\n", res) ;/*
 printf ("==========================================================\n") ;
 printf (" |               FLOP_TSC - FLOAT               |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init (vec1, 1.0) ;
     
     start_tsc = _rdtsc () ;
        res =  mnblas_sasum (VECSIZE, vec1, 1) ;
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("sasum nano ", 2*VECSIZE, end_tsc-start_tsc) ;
   }

 printf ("res = %f\n", res) ;
  printf ("==========================================================\n") ;
 printf (" |               FLOP_TSC - DOUBLE                       |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init_double (vec1d, 1.0) ;
     
     start_tsc = _rdtsc () ;
        res =  mnblas_dasum (VECSIZE, vec1d, 1) ;
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("dasum nano ", 2*VECSIZE, end_tsc-start_tsc) ;
   }

 printf ("res = %f\n", res) ;*/
 printf ("==========================================================\n") ;
 printf (" |              FLOP_MICRO -  FLOAT              |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init (vec1, 1.0);
     
     TOP_MICRO(start) ;
        res = mnblas_sasum (VECSIZE, vec1, 1) ;
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("sasum micro", 2*VECSIZE, tdiff_micro (&start, &end)) ;
   }

 printf ("res = %f\n", res) ;
  printf ("==========================================================\n") ;
 printf (" |              FLOP_MICRO -  DOUBLE              |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init_double (vec1d, 1.0);
     
     TOP_MICRO(start) ;
        res = mnblas_dasum (VECSIZE, vec1d, 1) ;
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("dasum micro", 2*VECSIZE, tdiff_micro (&start, &end)) ;
   }

 printf ("res = %f\n", res) ;/*
 printf ("==========================================================\n") ;
 printf (" |           FLOP_TSC - COMPLEXE FLOAT          |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init (vec3, 1.0, 2.0);
     
     start_tsc = _rdtsc () ;
        res = mnblas_scasum (VECSIZE, vec3, 1) ;
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("scasum nano: ", 4 * VECSIZE, end_tsc-start_tsc) ;
   }

 printf ("res = %f\n", res) ;
  printf ("==========================================================\n") ;
 printf (" |           FLOP_TSC - COMPLEXE DOUBLE          |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init_double (vec3d, 1.0, 2.0);
     
     start_tsc = _rdtsc () ;
        res = mnblas_dzasum (VECSIZE, vec3d, 1) ;
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("dzasum nano: ", 4 * VECSIZE, end_tsc-start_tsc) ;
   }

 printf ("res = %f\n", res) ;*/
 printf ("==========================================================\n") ;
 printf (" |         FLOP_MICRO - COMPLEXE FLOAT         |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init (vec3, 1.0, 2.0);
     
     TOP_MICRO(start) ;
        res = mnblas_scasum (VECSIZE, vec3, 1) ;
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("scasum micro: ", 4 * VECSIZE, tdiff_micro (&start, &end)) ;
   }

 printf ("res = %f\n", res) ;
  printf ("==========================================================\n") ;
 printf (" |         FLOP_MICRO - COMPLEXE DOUBLE         |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init_double (vec3d, 1.0, 2.0);
     
     TOP_MICRO(start) ;
        res = mnblas_dzasum (VECSIZE, vec3d, 1) ;
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("dzasum micro: ", 4 * VECSIZE, tdiff_micro (&start, &end)) ;
   }

 printf ("res = %f\n", res) ;
}

/*
  calul de la complexité pour un réel : on fait n fois une addition et un test if dont 2 n
  on sait qu'une addtion sur un complexe nécessite deux addtions et une multiplication 6 opération (4 multiplication et deux additions)
  on a donc pour un complexe: deux tests et deux addition classque donc la difficulté est de 4n
*/