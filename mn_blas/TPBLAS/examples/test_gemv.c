#include <stdio.h>

#include "mnblas.h"
#include "complexe.h"

#include "flop.h"

#define VECSIZE    2048
#define MATSIZE    VECSIZE*VECSIZE
#define NB_FOIS    10

typedef float vfloat [VECSIZE] ;
typedef double vdouble [VECSIZE] ;
typedef complexe_float_t vcomplexe [VECSIZE] ;
typedef complexe_double_t vcomplexedouble [VECSIZE] ;
typedef float Mfloat [MATSIZE];
typedef double Mdouble [MATSIZE];
typedef complexe_float_t Mcomplexe [MATSIZE] ;
typedef complexe_double_t Mcomplexe_double [MATSIZE] ;

vfloat vec1, vec2 ;
vdouble vec1d, vec2d ;
Mfloat mat1;
Mdouble mat1d;
vcomplexe vec3, vec4 ;
vcomplexedouble vec3d,vec4d;
Mcomplexe mat2;
Mcomplexe_double mat2d;

void vector_init (vfloat V, float x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    V [i] = x ;

  return ;
}

void matrice_init (Mfloat M, float x)
{
  register unsigned int i ;
  register unsigned int j ;
  for (j = 0; j<VECSIZE; j++){
    for (i = 0; i < VECSIZE; i++)
        M [i+j*VECSIZE] = x ;
  }
  return ;
}

void vector_init_double (vdouble V, double x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    V [i] = x ;

  return ;
}

void matrice_init_double (Mdouble M, double x)
{
  register unsigned int i ;
  register unsigned int j ;
  for (j = 0; j<VECSIZE; j++){
    for (i = 0; i < VECSIZE; i++)
        M [i+j*VECSIZE] = x ;
  }
  return ;
}

void vector_complexe_init (vcomplexe V, float a, float b)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++){
    V[i].real = a ;
    V[i].imaginary = b ;
  }
  return ;
}

void matrice_complexe_init (Mcomplexe M, float a, float b)
{
  register unsigned int i ;
  register unsigned int j ;

for (j = 0; j < VECSIZE;j++){
  for (i = 0; i < VECSIZE; i++){
    M[i+j*VECSIZE].real = a ;
    M[i+j*VECSIZE].imaginary = b ;
  }
}
  return ;
}

void vector_complexe_init_double (vcomplexedouble V, double a, double b)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++){
    V[i].real = a ;
    V[i].imaginary = b ;
  }
  return ;
}


void matrice_complexe_init_double (Mcomplexe_double M, double a, double b)
{
  register unsigned int i ;
  register unsigned int j ;

for (j = 0; j < VECSIZE;j++){
  for (i = 0; i < VECSIZE; i++){
    M[i+j*VECSIZE].real = a ;
    M[i+j*VECSIZE].imaginary = b ;
  }
}
  return ;
}

void vector_print (vfloat V)
{
  register unsigned int i = 0;

  //for (i = 0; i < VECSIZE; i++)
    printf ("%f ", V[i]) ;
  printf ("\n") ;
  
  return ;
}

void vector_complexe_print (vcomplexe V){
  register unsigned int i = 0;

  //for (i = 0; i < VECSIZE; i++){
    printf ("%f ", V[i].real) ;
    printf ("%f ", V[i].imaginary) ;
  //}
  printf ("\n") ;
  
  return ;
}

int main (int argc, char **argv)
{
 struct timeval start, end ;
 //unsigned long long int start_tsc, end_tsc ;

 int i ;
 printf ("==========================================================\n") ;
 printf (" |         TEST FONCTIONNALITE - DOUBLE / FLOAT         |\n") ;
 printf ("==========================================================\n") ;
  // test sur des petites structures
  float matrice[4]={1,1,1,1};
  float a=2;
  float vector[2]={1,1};
  float b=1;
  float res[2]={1,1};
  mncblas_sgemv(101,111,2,2,a,matrice,2,vector,1,b,res,1);
  vector_print(res);
  

 printf ("==========================================================\n") ;
 printf (" |     TEST FONCTIONNALITE - COMPLEXE DOUBLE / FLOAT     |\n") ;
 printf ("==========================================================\n") ;
  // test sur des petites structures
  complexe_float_t matricec[4];
  matricec[0].real=1;
  matricec[1].real=1;
  matricec[2].real=1;
  matricec[3].real=1;
  matricec[0].imaginary=0;
  matricec[1].imaginary=0;
  matricec[2].imaginary=0;
  matricec[3].imaginary=0;
  complexe_float_t c1 = {2.0, 0.0};
  complexe_float_t vectorc[2];
  vectorc[0].real=1;
  vectorc[1].real=1;
  vectorc[0].imaginary=0;
  vectorc[1].imaginary=0;
  complexe_float_t resc[2];
  resc[0].real=1;
  resc[1].real=1;
  resc[0].imaginary=0;
  resc[1].imaginary=0;
  complexe_float_t c2 = {1.0, 0.0};
  mncblas_cgemv(101,111,2,2,&c1,matricec,2,vectorc,1,&c2,resc,1);
  vector_complexe_print(resc);
/*
 printf ("==========================================================\n") ;
 printf (" |               FLOP_TSC - FLOAT               |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init (vec1, 1.0) ;
     vector_init (vec2, 2.0) ;
     matrice_init(mat1,1.0);
     float alpha=1;
     float beta=1;

     start_tsc = _rdtsc () ;
      mncblas_sgemv(101,111,VECSIZE,VECSIZE,alpha,mat1,VECSIZE,vec1,1,beta,vec2,1);
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("sgemv nano ", 3 * MATSIZE+2*VECSIZE, end_tsc-start_tsc) ;
   }

 printf ("==========================================================\n") ;
 printf (" |               FLOP_TSC - DOUBLE               |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init_double (vec1d, 1.0) ;
     vector_init_double (vec2d, 2.0) ;
     matrice_init_double(mat1d,1.0);
     float alpha=1;
     float beta=1;

     start_tsc = _rdtsc () ;
      mncblas_dgemv(101,111,VECSIZE,VECSIZE,alpha,mat1d,VECSIZE,vec1d,1,beta,vec2d,1);
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("dgemv nano ", 3 * MATSIZE+2*VECSIZE, end_tsc-start_tsc) ;
   }
*/
 printf ("==========================================================\n") ;
 printf (" |              FLOP_MICRO - FLOAT              |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init (vec1, 1.0) ;
     vector_init (vec2, 2.0) ;
     matrice_init(mat1,1.0);
     float alpha=1;
     float beta=1;
     
     TOP_MICRO(start) ;
             mncblas_sgemv(101,111,VECSIZE,VECSIZE,alpha,mat1,VECSIZE,vec1,1,beta,vec2,1);
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("sgemv micro", 3 * MATSIZE+2*VECSIZE, tdiff_micro (&start, &end)) ;
   }

 printf ("==========================================================\n") ;
 printf (" |              FLOP_MICRO - DOUBLE               |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init_double (vec1d, 1.0) ;
     vector_init_double (vec2d, 2.0) ;
     matrice_init_double(mat1d,1.0);
     float alpha=1;
     float beta=1;
     
     TOP_MICRO(start) ;
             mncblas_dgemv(101,111,VECSIZE,VECSIZE,alpha,mat1d,VECSIZE,vec1d,1,beta,vec2d,1);
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("dgemv micro", 3 * MATSIZE+2*VECSIZE, tdiff_micro (&start, &end)) ;
   }
/*
 printf ("==========================================================\n") ;
 printf (" |           FLOP_TSC - COMPLEXE FLOAT          |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init (vec3, 3.0, 5.0);
     vector_complexe_init (vec4, 4.0, 6.0);
     matrice_complexe_init(mat2,1.0,1.0);
     complexe_float_t alpha = {2.0, 0.0};
     complexe_float_t beta = {1.0, 0.0};
     start_tsc = _rdtsc () ;
     mncblas_cgemv(101,111,VECSIZE,VECSIZE,&alpha,mat2,VECSIZE,vec3,1,&beta,vec4,1);
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("cgemv nano: ", 10 * MATSIZE+8*VECSIZE, end_tsc-start_tsc) ;
   }

 printf ("==========================================================\n") ;
 printf (" |           FLOP_TSC - COMPLEXE DOUBLE          |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init_double (vec3d, 3.0, 5.0);
     vector_complexe_init_double (vec4d, 4.0, 6.0);
     matrice_complexe_init_double(mat2d,1.0,1.0);
     complexe_float_t alpha = {2.0, 0.0};
     complexe_float_t beta = {1.0, 0.0};
     start_tsc = _rdtsc () ;
     mncblas_zgemv(101,111,VECSIZE,VECSIZE,&alpha,mat2d,VECSIZE,vec3d,1,&beta,vec4d,1);
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("zgemv nano: ", 10 * MATSIZE+8*VECSIZE, end_tsc-start_tsc) ;
   }
*/
 printf ("==========================================================\n") ;
 printf (" |         FLOP_MICRO - COMPLEXE FLOAT         |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init (vec3, 3.0, 5.0);
     vector_complexe_init (vec4, 4.0, 6.0);
     matrice_complexe_init(mat2,1.0,1.0);
     complexe_float_t alpha = {2.0, 0.0};
     complexe_float_t beta = {1.0, 0.0};
     TOP_MICRO(start) ;
     mncblas_cgemv(101,111,VECSIZE,VECSIZE,&alpha,mat2,VECSIZE,vec3,1,&beta,vec4,1);
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("cgemv micro: ", 10 * MATSIZE+8*VECSIZE, tdiff_micro (&start, &end)) ;
   }

 printf ("==========================================================\n") ;
 printf (" |         FLOP_MICRO - COMPLEXE DOUBLE        |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init_double (vec3d, 3.0, 5.0);
     vector_complexe_init_double (vec4d, 4.0, 6.0);
     matrice_complexe_init_double(mat2d,1.0,1.0);
     complexe_float_t alpha = {2.0, 0.0};
     complexe_float_t beta = {1.0, 0.0};
     TOP_MICRO(start) ;
     mncblas_zgemv(101,111,VECSIZE,VECSIZE,&alpha,mat2d,VECSIZE,vec3d,1,&beta,vec4d,1);
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("zgemv micro: ", 10 * MATSIZE+8*VECSIZE, tdiff_micro (&start, &end)) ;
   }
}
