#include <stdio.h>

#include "mnblas.h"
#include "complexe.h"

#include "flop.h"

#define VECSIZE    65536

#define NB_FOIS    10

typedef float vfloat [VECSIZE] ;
typedef double vdouble [VECSIZE] ;
typedef complexe_float_t vcomplexe [VECSIZE] ;
typedef complexe_double_t vcomplexedouble [VECSIZE] ;

vfloat vec1;
vdouble vec1d ;
vcomplexe vec3 ;
vcomplexedouble vec3d;

void vector_init (vfloat V, float x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    V [i] = x ;

  return ;
}

void vector_init_double (vdouble V, double x)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++)
    V [i] = x ;

  return ;
}

void vector_complexe_init (vcomplexe V, float a, float b)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++){
    V[i].real = a ;
    V[i].imaginary = b ;
  }
  return ;
}

void vector_complexe_init_double (vcomplexedouble V, double a, double b)
{
  register unsigned int i ;

  for (i = 0; i < VECSIZE; i++){
    V[i].real = a ;
    V[i].imaginary = b ;
  }
  return ;
}

void vector_print (vfloat V)
{
  register unsigned int i = 0;

  for (i = 0; i < VECSIZE; i++)
    printf ("\n%d: %f ", i, V[i]) ;
  printf ("\n") ;
  
  return ;
}

void vector_complexe_print (vcomplexe V){
  register unsigned int i = 0;

  for (i = 0; i < VECSIZE; i++){
    printf ("\n%d: %f %f", i, V[i].real, V[i].imaginary) ;
  }
  printf ("\n") ;
  
  return ;
}

int main (int argc, char **argv)
{
 struct timeval start, end ;
 //unsigned long long int start_tsc, end_tsc ;
 
 CBLAS_INDEX res ;
 int i ;
 printf ("==========================================================\n") ;
 printf (" |         TEST FONCTIONNALITE - DOUBLE / FLOAT         |\n") ;
 printf ("==========================================================\n") ;
  
  vector_init (vec1, 1.0);
  vec1[VECSIZE/2] = 2.0;

  printf("Vec1 = ");
  vector_print(vec1);
  printf("*** Utilisation de la fonction mnblas_isamin...\n");
    res = mnblas_isamin (VECSIZE, vec1, 1);
  //printf("Vec1 = %f\n", *vec1);

 printf ("res = %ld\n", res) ;  //indice du min de Vec1
 printf ("==========================================================\n") ;
 printf (" |     TEST FONCTIONNALITE - COMPLEXE DOUBLE / FLOAT     |\n") ;
 printf ("==========================================================\n") ;
  
  vector_complexe_init (vec3, 1.0, 2.0);
  vec3[VECSIZE/2].real = 2.0;
  
  printf("Vec3 = ");
  vector_complexe_print(vec3);
  printf("*** Utilisation de la fonction mnblas_icamin...\n");
  res = mnblas_icamin (VECSIZE, vec3, 1);
  //printf("Vec3 = %f %f\n", vec3->real, vec3->imaginary);

 printf ("res = %ld\n", res) ;  //indice du min de Vec3
 /*
 printf ("==========================================================\n") ;
 printf (" |               FLOP_TSC - FLOAT               |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init (vec1, 1.0) ;
     vec1[VECSIZE/2] = 2.0;
     
     start_tsc = _rdtsc () ;
        res =  mnblas_isamin (VECSIZE, vec1, 1) ;
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("isamin nano ", 1 * VECSIZE, end_tsc-start_tsc) ;
   }

 printf ("res = %ld\n", res) ;

  printf ("==========================================================\n") ;
 printf (" |               FLOP_TSC - DOUBLE               |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init_double (vec1d, 1.0) ;
     vec1[VECSIZE/2] = 2.0;
     
     start_tsc = _rdtsc () ;
        res =  mnblas_idamin (VECSIZE, vec1d, 1) ;
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("idamin nano ", 1* VECSIZE, end_tsc-start_tsc) ;
   }

 printf ("res = %ld\n", res) ;*/
 printf ("==========================================================\n") ;
 printf (" |              FLOP_MICRO -  FLOAT              |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init (vec1, 1.0);
     vec1[VECSIZE/2] = 2.0;
     
     TOP_MICRO(start) ;
        res = mnblas_isamin (VECSIZE, vec1, 1) ;
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("isamin micro", 1 * VECSIZE, tdiff_micro (&start, &end)) ;
   }

 printf ("res = %ld\n", res) ;

 printf ("==========================================================\n") ;
 printf (" |              FLOP_MICRO -  DOUBLE              |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_init_double (vec1d, 1.0);
     vec1[VECSIZE/2] = 2.0;
     
     TOP_MICRO(start) ;
        res = mnblas_idamin (VECSIZE, vec1d, 1) ;
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("idamin micro", 1 * VECSIZE, tdiff_micro (&start, &end)) ;
   }

 printf ("res = %ld\n", res) ;/*
 printf ("==========================================================\n") ;
 printf (" |           FLOP_TSC - COMPLEXE FLOAT          |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init (vec3, 1.0, 2.0);
     vec3[VECSIZE/2].real = 2.0;
     
     start_tsc = _rdtsc () ;
        res = mnblas_icamin (VECSIZE, vec3, 1) ;
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("icamin nano: ", 2 * VECSIZE, end_tsc-start_tsc) ;
   }

 printf ("res = %ld\n", res) ;
  printf ("==========================================================\n") ;
 printf (" |           FLOP_TSC - COMPLEXE DOUBLE        |\n") ;
 printf ("==========================================================\n") ;

 init_flop_tsc () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init_double (vec3d, 1.0, 2.0);
     vec3[VECSIZE/2].real = 2.0;
     
     start_tsc = _rdtsc () ;
        res = mnblas_izamin (VECSIZE, vec3d, 1) ;
     end_tsc = _rdtsc () ;
     
     calcul_flop_tsc ("izamin nano: ", 2 * VECSIZE, end_tsc-start_tsc) ;
   }

 printf ("res = %ld\n", res) ;*/
 printf ("==========================================================\n") ;
 printf (" |         FLOP_MICRO - COMPLEXE FLOAT         |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init (vec3, 1.0, 2.0);
     vec3[VECSIZE/2].real = 2.0;
     
     TOP_MICRO(start) ;
        res = mnblas_icamin (VECSIZE, vec3, 1) ;
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("icamin micro: ", 2 * VECSIZE, tdiff_micro (&start, &end)) ;
   }

 printf ("res = %ld\n", res) ;

 printf ("==========================================================\n") ;
 printf (" |         FLOP_MICRO - COMPLEXE DOUBLE        |\n") ;
 printf ("==========================================================\n") ;

 init_flop_micro () ;
 
 for (i = 0 ; i < NB_FOIS; i++)
   {
     vector_complexe_init_double (vec3d, 1.0, 2.0);
     vec3[VECSIZE/2].real = 2.0;
     
     TOP_MICRO(start) ;
        res = mnblas_izamin (VECSIZE, vec3d, 1) ;
     TOP_MICRO(end) ;
     
     calcul_flop_micro ("izamin micro: ", 2 * VECSIZE, tdiff_micro (&start, &end)) ;
   }

 printf ("res = %ld\n", res) ;
}

/*
* Pour le calcul de la complexité
* pour un élément simple : on fait une comparaison du v[i] avec le min à l'instant i que l'on fait N fois
* pour un élément complexe: fait une somme de l'imaginaire et du complexe puis les compare avec le min dont 2 N opérations
*
*/