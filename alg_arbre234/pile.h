#ifndef GEN_H
#define GEN_H

#define MAX_PILE_SIZE       50


typedef struct {
  int sommet ;
  cell Tab [100] ;
} pile_t, *ppile_t ;

ppile_t creer_pile () ;

int detruire_pile (ppile_t p) ;

int pile_vide (ppile_t p) ;

int pile_pleine (ppile_t p) ;

cell depiler (ppile_t p)  ;

int empiler (ppile_t p, cell pn) ;


#endif
