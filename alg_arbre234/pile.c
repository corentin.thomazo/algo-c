#include <stdlib.h>
#include "a234.h"
#include "pile.h"

ppile_t creer_pile ()
{
  ppile_t p = malloc(sizeof(pile_t));
  p->sommet=-1;
  return p;
}

int detruire_pile (ppile_t p)
{
  free(p);
  return 0;
}  

  /*  true: pile vide | false: pile non vide  */
int pile_vide (ppile_t p)
{
  if(p->sommet==-1)
    return 1;
  return 0;
}
  /*  true: pile pleine | false: pile non pleine  */
int pile_pleine (ppile_t p)
{
  if(p->sommet>=MAX_PILE_SIZE)
    return 1;
  return 0;
} 

cell depiler (ppile_t p)
{
 cell elem = {0,NULL};
 int pileEstVide = pile_vide(p);
 if(pileEstVide==0){
  elem=p->Tab[p->sommet];
  p->sommet-=1;
 }
 return elem;
}

int empiler (ppile_t p, cell pn)
{
int pileEstPleine = pile_pleine(p);
 if(pileEstPleine<MAX_PILE_SIZE){
  p->sommet+=1;
  p->Tab[p->sommet]=pn;
  return 1;
 }
 return 0;
}
