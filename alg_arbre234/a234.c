#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "a234.h"
#include "file.h"
#include "pile.h"

#define max(a,b) ((a)>(b)?(a):(b))

/*
* param : Arbre234 a
* return: un entier
* fonction: retourne la hauteur de l'arbre avec un parcours en profondeur
*/
int hauteur (Arbre234 a)
{
  int h0, h1, h2, h3 ;
  
  if (a == NULL)
    return 0 ;

  if (a->t == 0)
    return 0 ;

  h0 = hauteur (a->fils [0]) ;
  h1 = hauteur (a->fils [1]) ;
  h2 = hauteur (a->fils [2]) ;
  h3 = hauteur (a->fils [3]) ;

  return 1 + max (max (h0,h1),max (h2,h3)) ;
} 

/*
* param : Arbre234 a
* return: un entier
* fonction: retourne le nombre de clés de l'arbre avec un parcours en profondeur
*/

int NombreCles (Arbre234 a)
{
  int n0=0,n1=0,n2=0,n3=0;
if (a == NULL)
    return 0 ;

  if (a->t == 0)
    return 0 ;
  n1 = NombreCles (a->fils [1]) ;
  n2 = NombreCles (a->fils [2]) ;
  if(a->t>2){
    n0 = NombreCles (a->fils [0]) ;
  }
  if(a->t>3){
    n3 = NombreCles (a->fils [3]) ;
  }
  return a->t+n0+n1+n2+n3-1 ;
}

/*
* param : Arbre234 a
* return: un entier
* fonction: retourne la clé max de l'arbre avec un parcours en profondeur
*/

int CleMax (Arbre234 a)
{
  if (a == NULL)
    return -1 ;

  if (a->t == 0)
    return -1 ;

  if (a->t==2){
    if (a->fils[2]->t!=0){
      return CleMax (a->fils[2]);
    }else{
      return a->cles[1];
    }
  }

  if (a->fils[a->t-1]->t!=0){
    return CleMax (a->fils[a->t-1]);
  }else{
    return a->cles[a->t-2];
  }
}

/*
* param : Arbre234 a
* return: un entier
* fonction: retourne la clé min de l'arbre avec un parcours en profondeur
*/

int CleMin (Arbre234 a)
{
  if (a == NULL)
    return -1 ;

  if (a->t == 0)
    return -1 ;
  if (a->t==2){
    if (a->fils[1]->t!=0){
      return CleMin (a->fils[1]);
    }else{
      return a->cles[1];
    }
  }
  if (a->fils[0]->t!=0){
      return CleMin (a->fils[1]);
  }else{
      return a->cles[0];
  }
  
}

/*
* param : Arbre234 a
* return: Arbre234
* fonction: retourne le noeud avec la clés rechercher avec un parcours en profondeur
* si la clé n'est pas présente renvoie NULL
*/

Arbre234 RechercherCle (Arbre234 a, int cle)
{
  if (a->t==0){
    return NULL;
  }
  if(a->t==2){
    
    if (a->cles[1]==cle){
      return a;
    }
    if(a->cles[1]>cle){
      return RechercherCle(a->fils[1],cle);
    }else{
      return RechercherCle(a->fils[2],cle);
    }
  
  }else{
    if (cle==a->cles[0]){
      return a;
    }
    if (cle<a->cles[0]){
      return RechercherCle (a->fils[0],cle);
    }
    if (cle>a->cles[a->t-2]){
      return RechercherCle (a->fils[a->t-1],cle);
    }
    for(int i=3; i<=a->t;i++){
      if (a->cles[i-2]==cle){
        return a;
      }
      if((a->cles[i-3]<cle)&&(a->cles[i-2]>cle)){
        return RechercherCle (a->fils[i-2],cle);
      }
    
    }
  }
 return NULL;
}
/*
* param : Arbre234 a et 4 pointeurs d'entier
* return: un void
* fonction: détaille la structure de l'arbre avec un parcours en profondeur
*/
void AnalyseStructureArbre (Arbre234 a, int *feuilles, int *noeud2, int *noeud3, int *noeud4)
{
  if (a==NULL){
    return;
  }
  if (a->t==0){
    *feuilles+=1;
    return;
  }
  if (a->t==2){
    *noeud2 +=1;
  }
  if (a->t==3){
    *noeud3 +=1;
  }
  if (a->t==4){
    *noeud4 +=1;
  }
  AnalyseStructureArbre (a->fils[1],feuilles,noeud2,noeud3,noeud4);
  AnalyseStructureArbre (a->fils[2],feuilles,noeud2,noeud3,noeud4);
  if(a->t>=3){
    AnalyseStructureArbre (a->fils[0],feuilles,noeud2,noeud3,noeud4);
  }
  if (a->t==4){ 
    AnalyseStructureArbre (a->fils[3],feuilles,noeud2,noeud3,noeud4);
  }
}

/*
* param : Arbre234 a
* return: Arbre234
* fonction: retourne le noeud max de l'arbre avec un parcours en profondeur
*/
Arbre234 noeud_max (Arbre234 a)
{
  if(a==NULL||a==0){
    return NULL;
  }
  Arbre234 fils1,fils2,fils3,fils4;
  Arbre234 max;
  int n1,n2,n3,n4;
  int nmax;
  max=a;
  nmax=max->cles[0]+max->cles[1]+max->cles[2];
  fils1 = noeud_max(a->fils[0]);
  if (fils1!=NULL){
    n1=fils1->cles[0]+fils1->cles[1]+fils1->cles[2];
    if(max(nmax,n1)!=nmax){
      max=fils1;
      nmax=n1;
    }
  }
  fils2 = noeud_max(a->fils[1]);
  if (fils2!=NULL){
    n2=fils2->cles[0]+fils2->cles[1]+fils2->cles[2];
    if(max(nmax,n2)!=nmax){
      max=fils2;
      nmax=n2;
    }
  }
  fils3 = noeud_max(a->fils[2]);
  if (fils3!=NULL){
    n3=fils3->cles[0]+fils3->cles[1]+fils3->cles[2];
    if(max(nmax,n3)!=nmax){
      max=fils3;
      nmax=n3;
    }
  }
  fils4 = noeud_max(a->fils[3]);
  if (fils4!=NULL){
    n4=fils4->cles[0]+fils4->cles[1]+fils4->cles[2];
    if(max(nmax,n4)!=nmax){
      max=fils4;
      nmax=n4;

    }
  }
  
  return max;
}

/*
* param : Arbre234 a
* return: void
* fonction: affiche les clés avec un parcours en largeur
*/

void Afficher_Cles_Largeur (Arbre234 a)
{
  pfile_t f1 = creer_file();
  enfiler(f1,a);
  while(!file_vide(f1)){
    Arbre234 temp=defiler(f1);
    if (temp->t!=0){
      printf("\n");
      if (temp->t==2){
        printf("%d ",temp->cles[1]);
        enfiler(f1,temp->fils[1]);
        enfiler(f1,temp->fils[2]);
      }else{
        printf("%d ",temp->cles[0]);
        printf("%d ",temp->cles[1]);
        enfiler(f1,temp->fils[0]);
        enfiler(f1,temp->fils[1]);
        enfiler(f1,temp->fils[2]);
        if (temp->t==4){
          printf("%d ",temp->cles[2]);
          enfiler(f1,temp->fils[3]);          
        }
      }
    }
  }

  return ;
}

/*
* param : Arbre234 a
* return: void
* fonction: affiche les clés avec un parcours en profondeur
*/

void Affichage_Cles_Triees_Recursive (Arbre234 a)
{
  if(a->t!=0){
  if (a->t==2){
    Affichage_Cles_Triees_Recursive(a->fils[1]);
    printf("%d  ",a->cles[1]);
    Affichage_Cles_Triees_Recursive(a->fils[2]);
  }else{
    Affichage_Cles_Triees_Recursive(a->fils[0]);
    printf("%d  ",a->cles[0]);
    Affichage_Cles_Triees_Recursive(a->fils[1]);
    printf("%d  ",a->cles[1]);
    Affichage_Cles_Triees_Recursive(a->fils[2]);
    if (a->t==4){
      printf("%d  ",a->cles[2]);
    Affichage_Cles_Triees_Recursive(a->fils[3]);
    }
  }
  }
}

/*
* param : Arbre234 a
* return: void
* fonction: affiche les clés trié avec l'utilisation d'une pile
*/

void Affichage_Cles_Triees_NonRecursive (Arbre234 a)
{
  ppile_t p=creer_pile();
  cell temporaire;
  // permet d'enregistrer le nombre de passage dans le noeud lors de l'affichage
  temporaire.num=0;
  
  temporaire.mem=a;
  
  while(temporaire.mem->t!=0){
    empiler(p,temporaire);
    if (temporaire.mem->t!=0){
      if (temporaire.mem->t==2){
        temporaire.mem=temporaire.mem->fils[1];
      }else{
        temporaire.mem=temporaire.mem->fils[0];
      }
    }
  }
  
  while(!pile_vide(p)){
    cell sortie=depiler(p);
    if (sortie.mem->t!=0){
      if (sortie.mem->t==2){
        printf("%d  ",sortie.mem->cles[1]);
        if(sortie.mem->fils[2]!=NULL){
          temporaire.mem=sortie.mem->fils[2];
          while(temporaire.mem->t!=0){
            empiler(p,temporaire);
            if (temporaire.mem->t!=0){
              if (temporaire.mem->t==2){
                temporaire.mem=temporaire.mem->fils[1];
              }else{
                temporaire.mem=temporaire.mem->fils[0];
              }
            }
          }
        }
      }else{
        if (sortie.num<sortie.mem->t-1){
          printf("%d  ",sortie.mem->cles[sortie.num]);
          sortie.num+=1;
          if (sortie.num<sortie.mem->t){
            if(sortie.mem->fils[sortie.num]!=NULL){
              empiler(p,sortie);
              temporaire.mem=sortie.mem->fils[sortie.num];
              while(temporaire.mem->t!=0){
                empiler(p,temporaire);
                if (temporaire.mem->t!=0){
                  if (temporaire.mem->t==2){
                    temporaire.mem=temporaire.mem->fils[1];
                  }else{
                    temporaire.mem=temporaire.mem->fils[0];
                  }
                }
              }
            }
          } 
        }
      }
    } 
  }
}


// header détruire clé//
void Detruire_Cle (Arbre234 *a, int cle);
Arbre234 cherche(Arbre234 a,int cle,ppile_t pile);
Arbre234 remonter(Arbre234 a, ppile_t pile);
Arbre234 reequilibragegauche(Arbre234 parent,int i);
Arbre234 reequilibragedroite(Arbre234 parent ,int i);
Arbre234 reequilibremerge(Arbre234 parent,int i);
Arbre234 rotationg(Arbre234 parent,Arbre234 a,int i);
Arbre234 rotationd(Arbre234 parent,Arbre234 a,int i);
Arbre234 merge(Arbre234 parent,int i);
Arbre234 leaf(Arbre234 a,int cle,ppile_t pile);
Arbre234 findmin(Arbre234 a);
Arbre234 swap(Arbre234 a,Arbre234 min,int i);
Arbre234 merge_1_1_1(Arbre234 a);
Arbre234 cherche(Arbre234 a,int cle,ppile_t pile);
//FIN HEADER//

/*
* param : Arbre234 a , un entier i
* return: Arbre234
* fonction: rééquilibre les hauteur du fils i avec une rotation avec le fils i-1
*/

Arbre234 reequilibragegauche(Arbre234 parent,int i){
  // pb 
  int retenue= parent->cles[i-1];
  if (parent->fils[i-1]->t==3){
        parent->fils[i-1]->t=2;
        parent->cles[i-1]=parent->fils[i-1]->cles[1];
        parent->fils[i-1]->cles[1]=parent->fils[i-1]->cles[0];
        Arbre234 NewNoeud=malloc(sizeof(noeud234));
        NewNoeud->t=2;
        NewNoeud->cles[1]=retenue;
        NewNoeud->fils[2]=parent->fils[i];
        NewNoeud->fils[1]=parent->fils[i-1]->fils[2];
        parent->fils[i]=NewNoeud;
        parent->fils[i-1]->fils[2]=parent->fils[i-1]->fils[1];
        parent->fils[i-1]->fils[1]=parent->fils[i-1]->fils[0];
        return parent;
      }else{
        parent->fils[i-1]->t=3;
        parent->cles[i-1]=parent->fils[i-1]->cles[2];
        Arbre234 NewNoeud=malloc(sizeof(noeud234));
        NewNoeud->t=2;
        NewNoeud->cles[1]=retenue;
        NewNoeud->fils[2]=parent->fils[i];
        NewNoeud->fils[1]=parent->fils[i-1]->fils[4];
        parent->fils[i]=NewNoeud;
        return parent;
      }

}

/*
* param : Arbre234 a , un entier i
* return: Arbre234
* fonction: rééquilibre les hauteur du fils i avec une rotation avec le fils i+1
*/

Arbre234 reequilibragedroite(Arbre234 parent ,int i){ 
  //pb cles
  int retenue= parent->cles[i];
   if (parent->fils[i+1]->t==3){
        parent->fils[i+1]->t=2;
        parent->cles[i]=parent->fils[i+1]->cles[0];
        Arbre234 NewNoeud=malloc(sizeof(noeud234));
        NewNoeud->t=2;
        NewNoeud->cles[1]=retenue;
        NewNoeud->fils[1]=parent->fils[i];
        NewNoeud->fils[2]=parent->fils[i+1]->fils[0];
        parent->fils[i]=NewNoeud;
        return parent;
      }else{
        parent->fils[i+1]->t=3;
        parent->cles[i]=parent->fils[i+1]->cles[0];
        Arbre234 NewNoeud=malloc(sizeof(noeud234));
        NewNoeud->t=2;
        NewNoeud->cles[1]=retenue;
        NewNoeud->fils[1]=parent->fils[1];
        NewNoeud->fils[2]=parent->fils[2]->fils[0];
        parent->fils[i]=NewNoeud;
        parent->fils[i+1]->fils[0]=parent->fils[i+1]->fils[1];
        parent->fils[i+1]->fils[1]=parent->fils[i+1]->fils[2];
        parent->fils[i+1]->fils[2]=parent->fils[i+1]->fils[3];
        parent->fils[i+1]->cles[0]=parent->fils[i+1]->cles[1];
        parent->fils[i+1]->cles[1]=parent->fils[i+1]->cles[2];
        return parent;
      }
}

/*
* param : Arbre234 a , un entier i
* return: Arbre234
* fonction: rééquilibre les hauteur du fils i en faisant descendant la clé de parent dans un fils
*/

Arbre234 reequilibremerge(Arbre234 parent,int i){
  if(i<parent->t-1){
    Arbre234 frereg=parent->fils[i+1];
    frereg->t=3;
    frereg->cles[0]=parent->cles[i];
    frereg->fils[0]=parent->fils[i];
    if(parent->t==3){
      parent->t=2;
      if(i==1){
        parent->cles[1]=parent->cles[0];
        parent->fils[1]=parent->fils[0];
      }
    }
    if(parent->t==4){
      parent->t=3;
      for(int j=i; j<2;j++){
        parent->cles[j]=parent->cles[j+1];
        parent->fils[j]=parent->fils[j+1];
      }
      parent->fils[2]=parent->fils[3];
    }
    return parent;
  }else{
    parent->fils[i-1]->t=3;
    parent->fils[i-1]->cles[0]=parent->fils[i-1]->cles[1];
    parent->fils[i-1]->fils[0]=parent->fils[i-1]->fils[1];
    parent->fils[i-1]->fils[1]=parent->fils[i-1]->fils[2];
    parent->fils[i-1]->cles[1]=parent->cles[i-1];
    parent->fils[i-1]->fils[2]=parent->fils[i];
    parent->t-=1;
    if(parent->t==2){
      parent->cles[1]=parent->cles[0];
      parent->fils[2]=parent->fils[1];
      parent->fils[1]=parent->fils[0];

    }
    return parent;
  }

}

/*
* param : Arbre234 a , ppile_t pile
* return: Arbre234
* fonction: besoin d'équilibrer les hauteurs
* appelle le parent afin de faire un rééquilibrage avec les fonctions:
* reequilibragedroite, reequilibragegauche, reequilibremerge
*/

Arbre234 remonter(Arbre234 a, ppile_t pile){
  if (pile_vide(pile)){
    return NULL;
  }
  cell cellule=depiler(pile);
  Arbre234 parent= cellule.mem;
  int i= cellule.num;
  if (parent->t==2){
    if (i==2){
      if (parent->fils[1]->t==2){
        Arbre234 temp=malloc(sizeof(noeud234));
        temp=parent->fils[1];
        parent->t=3;
        parent->cles[0]=temp->cles[1];
        parent->fils[0]=temp->fils[1];
        parent->fils[1]=temp->fils[2];
        free(temp);
        return remonter(parent,pile); 
      }
    }
    if (i==1){
      if (parent->fils[2]->t==2){
        Arbre234 temp=malloc(sizeof(noeud234));
        temp=parent->fils[2];
        parent->t=3;
        parent->cles[0]=parent->cles[1];
        parent->cles[1]=temp->cles[1];
        parent->fils[0]=parent->fils[1];
        parent->fils[1]=temp->fils[1];
        parent->fils[2]=temp->fils[2];
        free(temp);
        return remonter(parent,pile); 
      }
    } 
    if (parent->fils[1]==a){
      return reequilibragedroite(parent,1);
    }else{
      if (parent->fils[2]==a){
        return reequilibragegauche(parent,2);
      }
    }
  }
  if(i==0){
    if(parent->fils[1]->t!=2){
      return reequilibragedroite(parent,0);
    }
    return reequilibremerge(parent,i);
  }
  if((i==2&&parent->t==3)||(i==3&&parent->t==4)){
    if(parent->fils[i-1]->t!=2){
      return reequilibragegauche(parent,i);
    }
    return reequilibremerge(parent,i);
  }
  if(parent->fils[i-1]->t!=2){
    return reequilibragegauche(parent,i);
  }else{
    if(parent->fils[i+1]->t!=2){
      return reequilibragedroite(parent,i);
    }
  }
  return reequilibremerge(parent,i);
}

/*
* param :Arbre234 parent, Arbre234 a , un entier i
* return: Arbre234
* fonction: fait une rotation gauche avec le fils i , la clé i et le fils i+1
*/

Arbre234 rotationg(Arbre234 parent,Arbre234 a,int i){

  if(parent->t!=2){
  a->cles[1]=parent->cles[i];
  parent->cles[i]=parent->fils[i+1]->cles[0];
  if (parent->fils[i+1]->t==3){
    parent->fils[i+1]->t=2;
  }else{
    parent->fils[i+1]->t=3;
    parent->fils[i+1]->cles[0]=parent->fils[i+1]->cles[1];
    parent->fils[i+1]->cles[1]=parent->fils[i+1]->cles[2];
  }
  }else{
    a->cles[1]=parent->cles[1];
      if (parent->fils[i+1]->t==3){
        parent->fils[i+1]->t=2;
        parent->cles[1]=parent->fils[i+1]->cles[0];
      }
      if (parent->fils[i+1]->t==4){
        parent->fils[i+1]->t=3;
        parent->cles[1]=parent->fils[i+1]->cles[0];
        parent->fils[i+1]->cles[0]=parent->fils[i+1]->cles[1];
        parent->fils[i+1]->cles[1]=parent->fils[i+1]->cles[2];
      }
  }
  return parent;
}

/*
* param :Arbre234 parent, Arbre234 a , un entier i
* return: Arbre234
* fonction: fait une rotation gauche avec le fils i, la clé i-1 et le fils i-1
*/

Arbre234 rotationd(Arbre234 parent,Arbre234 a,int i){

  if(parent->t!=2){
    a->cles[1]=parent->cles[i-1];
    if (parent->fils[i-1]->t==3){
      parent->cles[i-1]=parent->fils[i-1]->cles[1];
      parent->fils[i-1]->t=2;
      parent->fils[i-1]->cles[1]=parent->fils[i-1]->cles[0];
    }else{
      parent->cles[i-1]=parent->fils[i-1]->cles[2];
      parent->fils[i-1]->t=3;
    }
  }else{
    a->cles[1]=parent->cles[1];
      if (parent->fils[i-1]->t==3){
        parent->fils[i-1]->t=2;
        parent->cles[1]=parent->fils[i-1]->cles[1];
        parent->fils[i-1]->cles[1]=parent->fils[i-1]->cles[0];
      }
      if (parent->fils[i-1]->t==4){
        parent->fils[i-1]->t=3;
        parent->cles[1]=parent->fils[i-1]->cles[2];
      }
  }
  return parent;
}

/*
* param :Arbre234 parent , un entier i
* return: Arbre234
* fonction: fait un merge du noeud fils i et le fils i+1 et supprime la clé i de parent et l'ajoute dans le fils
*/

Arbre234 merge(Arbre234 parent,int i){
    free(parent->fils[i]);
    if (i==0&&parent->t==3){
      parent->t=parent->t-1;
      parent->fils[1]->t=3;
      parent->fils[1]->cles[0]=parent->cles[0];
      return parent;
    }
    if (i==1&&parent->t==3){
      parent->t=parent->t-1;
      parent->fils[0]->t=3;
       parent->fils[0]->cles[0]=parent->fils[0]->cles[1];
      parent->fils[0]->cles[1]=parent->cles[0];
      parent->fils[1]=parent->fils[0];
      return parent;
    }
    if (i==2&&parent->t==3){
      parent->t=2;
      parent->fils[2]=parent->fils[1];
      parent->fils[1]=parent->fils[0];
      parent->fils[2]->t=3;
      parent->fils[2]->cles[0]=parent->fils[2]->cles[1];
      parent->fils[2]->cles[1]=parent->cles[1];
      parent->cles[1]=parent->cles[0];
      return parent;
    }
     if (i==0&&parent->t==4){
      parent->t=parent->t-1;
      parent->fils[1]->t=3;
      parent->fils[1]->cles[0]=parent->cles[0];
      parent->cles[0]=parent->cles[1];
      parent->cles[1]=parent->cles[2];
      parent->fils[0]=parent->fils[1];
      parent->fils[1]=parent->fils[2];
      parent->fils[2]=parent->fils[3];
      return parent;
    }
    if (i==1&&parent->t==4){
      parent->t=parent->t-1;
      parent->fils[0]->t=3;
      parent->fils[0]->cles[0]=parent->fils[0]->cles[1];
      parent->fils[0]->cles[1]=parent->cles[0];
      parent->cles[0]=parent->cles[1];
      parent->cles[1]=parent->cles[2];
      parent->fils[1]=parent->fils[2];
      parent->fils[2]=parent->fils[3];
      
      return parent;
    }
    if(i==2&&parent->t==4){
    parent->t=3;
    parent->fils[2]=parent->fils[3];
    parent->fils[2]->t=3;
    parent->fils[2]->cles[0]=parent->cles[2];
    return parent;
    }
    if(i==3&&parent->t==4){
    parent->t=3;
    parent->fils[2]->t=3;
    parent->fils[2]->fils[2]=parent->fils[i];
    parent->fils[2]->cles[0]=parent->fils[2]->cles[1];
    parent->fils[2]->cles[1]=parent->cles[2];
    
    return parent;
    }
}

/*
* param :Arbre234 parent , un entier cle, ppile_t pile
* return: Arbre234
* fonction: dans le cas d'une feuille on supprime la clé et traite les différents cas
* Cas 1: le noeud a 2 ou 3 clé
* Cas 2: le noeud a une seule clé et donc on doit faire appelle à d'autre fonctions
*/

Arbre234 leaf(Arbre234 a,int cle,ppile_t pile){
  if (a->t!=2){ // cas simple où l'on enlève une clé d'un noeud
    if (a->t==3){
      if (a->cles[0]==cle){
        a->t=2;
      }else{
        a->t=2;
        a->cles[1]=a->cles[0];
      }
    }
    if(a->t==4){
      if (a->cles[0]==cle){
        a->t=3;
        a->cles[0]=a->cles[1];
        a->cles[1]=a->cles[2];
      }else {if (a->cles[1]==cle){
        a->t=3;
        a->cles[1]=a->cles[2];
        }else{
          a->t=3;
        }
      }
    }
    return a;    
  }else{
    // cas complexe où l'on doit s'intéresser au parent et aux frères
    cell cellule=depiler(pile);
    Arbre234 parent= cellule.mem;
    int i=cellule.num;
    if(parent->t!=2){ // cas où l'on va pouvoir directement modifier parent et frère
      if (i==0){
        if(parent->fils[1]->t!=2){
          return rotationg(parent,a,i);
        }else{
          return merge(parent,i);
        }
      }
      if ((i==2&&parent->t==3)||(i==3&&parent->t==4)){
        if(parent->fils[i-1]->t!=2){
          return rotationd(parent,a,i);
        }else{
          return merge(parent,i);
        }
      }
      if(parent->fils[i-1]->t!=2){
        return rotationd(parent,a,i);
      }else{
        if(parent->fils[i+1]->t!=2){
          return rotationg(parent,a,i);
        }else{
          return merge(parent,i);
        }
      }
    }else{
      // cas où l'on va avoir une modification de la hauteur
      if(parent->fils[1]->t==2&&parent->fils[2]->t==2){
        if (parent->fils[1]==a){
          parent->t=3;
          parent->cles[0]=parent->cles[1];
          parent->cles[1]=parent->fils[2]->cles[1];
        }
        if (parent->fils[2]==a){
          parent->t=3;
          parent->cles[0]=parent->fils[1]->cles[1];
        }
         Arbre234 feuille=parent->fils[1]->fils[1];
         feuille->t=0;
         
         if(parent->fils[1]!=NULL){
           free(parent->fils[1]);
         }
         if(parent->fils[2]!=NULL){
           free(parent->fils[2]);
         }
         for(int k=0; k<3;k++){
           if (parent->fils[k]!=NULL){
              parent->fils[k]=feuille;
           }
         }
          return remonter(parent,pile);
      }
      if (parent->fils[1]==a){
        i=1;
        if(parent->fils[2]->t!=2){
          return rotationg(parent,a,i);
        }
      }
      if (parent->fils[2]==a){
        i=2;
          if(parent->fils[1]->t!=2){
            return rotationd(parent,a,i);

          }
        }
      }

  } 
  return NULL; 
}

/*
* param :Arbre234 a
* return: Arbre234
* fonction: trouve le noeud minimal
*/

Arbre234 findmin(Arbre234 a){
  if (a->fils[1]->t==0){
    return a;
  }else{
    if(a->t==2){
      return findmin(a->fils[1]);
    }
    return findmin(a->fils[0]);
  }
}

/*
* param :Arbre234 a, Arbre234 min, entier i
* return: Arbre234
* fonction: échange la clé que l'on a la position i du noeud a et la clé minimal de min
*/

Arbre234 swap(Arbre234 a,Arbre234 min,int i){
 if (min->t==2){
   int temp =min->cles[1];
   min->cles[1]=a->cles[i];
   a->cles[i]=temp;
 }else{
   int temp =min->cles[0];
   min->cles[0]=a->cles[i];
   a->cles[i]=temp;
 }
 return a;
}

/*
* param :Arbre234 a, entier i, ppile_t pile
* return: Arbre234
* fonction: cas où la clé recherhcer est dans le noeud a 
* si c'est une feuille on appelle leaf
* sinon on cherche le minimum et on swap
*/

Arbre234 found(Arbre234 a,int cle,ppile_t pile){
  if (a->fils[1]->t==0){
    return leaf(a,cle,pile);
  }else{
    if (a->cles[0]==cle&&!(a->t==2)){
    Arbre234 min=findmin(a->fils[1]);
    swap(a,min,0);
    empiler(pile,(cell){1,a});
    cherche(a->fils[1],cle,pile);
    return a;
    }
    if (a->cles[1]==cle){
    Arbre234 min=findmin(a->fils[2]);
    swap(a,min,1);
    empiler(pile,(cell){2,a});
    cherche(a->fils[2],cle,pile);
    return a;
    }
    if (a->cles[2]==cle){
    Arbre234 min=findmin(a->fils[3]);
    swap(a,min,2);
    empiler(pile,(cell){3,a});
    cherche(a->fils[3],cle,pile);
    return a;
    }
  }


}

/*
* param :Arbre234 a
* return: Arbre234
* fonction: permet de merge le noeud racine et ses 2 fils
*/

Arbre234 merge_1_1_1(Arbre234 a){
  Arbre234 fg=a->fils[1];
  Arbre234 fd=a->fils[2];
  if (a->t==2&&fg->t==2&&fd->t==2){
    a->t=4;
    a->cles[0]=fg->cles[1];
    a->cles[2]=fd->cles[1];
    a->fils[0]=fg->fils[1];
    a->fils[1]=fg->fils[2];
    a->fils[2]=fd->fils[1];
    a->fils[3]=fd->fils[2];
  }
  return a;
}

/*
* param :Arbre234 a, int cle, ppile_t pile
* return: Arbre234
* fonction: recherche la clé dans l'Arbre234 a
* enregistre le chemin pris dans la pile où i représente le fils utilisé
* si il le trouve appelle found
* sinon appelle cherche dans les fils
*/

Arbre234 cherche(Arbre234 a,int cle,ppile_t pile){
  cell cellule= {0,a};
  if(a==NULL){
    return a;
  }
  if(a->fils[2]!=NULL){
    if(a->fils[2]->fils[2]){
    }
  }
  if(a->t==0){
    return a;
  }
  if (a->t==2){
    if(a->cles[1]==cle){
      return found(a,cle,pile);
    }else{
       // permettra de remonter l'arbre en cas de besoin
      if (a->cles[1]>cle){
        cellule.num=1;
        empiler(pile,cellule);
        return cherche(a->fils[1],cle,pile);
      }
      if (a->cles[1]<cle){
        cellule.num=2;
        empiler(pile,cellule);
        return cherche(a->fils[2],cle,pile);
      }
    }
  }
  if (a->cles[0]==cle||a->cles[1]==cle||(a->t==4&&a->cles[2]==cle)){
      return found(a,cle,pile);
      
  }
  if (cle<a->cles[0]){
    cellule.num=0;
    empiler(pile,cellule);
    return cherche(a->fils[0],cle,pile);
  }
  if (cle>a->cles[0]&&cle<a->cles[1]){
    cellule.num=1;
    empiler(pile,cellule);
    return cherche(a->fils[1],cle,pile);
  }
  if (cle>a->cles[1]&&a->t==3){
    cellule.num=2;
    empiler(pile,cellule);
    return cherche(a->fils[2],cle,pile);
  }
  if (cle>a->cles[1]&&cle<a->cles[2]){
    cellule.num=2;
    empiler(pile,cellule);
    return cherche(a->fils[2],cle,pile);
  }
  if (cle>a->cles[2]){
    cellule.num=3;
    empiler(pile,cellule);
    return cherche(a->fils[3],cle,pile);
  }
  return a;
}

/*
* param :Arbre234 a, int cle
* return: void
* fonction: détruit la clé dans l'Arbre234 a
* traite directement si a est un arbre de hauteur 1
* sinon fait appel à la fonction cherche
*/

void Detruire_Cle (Arbre234 *a, int cle)
{
  
  ppile_t pile=creer_pile();
  pnoeud234 c = *a ;
  if(c->fils[1]->t!=0){
  cherche(c,cle,pile);
  merge_1_1_1(c);
  return ;
  }else{
    if(c->t==2&&c->cles[1]==cle){
      c->t=0;
      if (c->fils[1]!=NULL){
        free(c->fils[1]);
      }
      if (c->fils!=NULL){
        free(c->fils[2]);
      }
    }else{
      if (c->t==3){
        if (c->cles[0]==cle){
          c->t=2;
        }
        if (c->cles[1]==cle){
          c->t=2;
          c->cles[1]=c->cles[0];
        }
      }
      if(c->t==4){
        if (c->cles[0]==cle){
          c->t=3;
          c->cles[0]=c->cles[1];
          c->cles[1]=c->cles[2];
        }
        if (c->cles[1]==cle){
          c->t=3;
          c->cles[1]=c->cles[2];
        }
        if (c->cles[2]==cle){
          c->t=3;

        }
      }
    }
  }
}

void stop(){
}

void affecter_tab_Cles_Largeur (Arbre234 a,int *tab)
{ 
  pfile_t f1 = creer_file();
  int i=0;
  enfiler(f1,a);
  while(!file_vide(f1)){
    Arbre234 temp=defiler(f1);
    if (temp!=NULL){
      if (temp->t!=0){
        if (temp->t==2){
          tab[i]=temp->cles[1];
          i++;
          enfiler(f1,temp->fils[1]);
          enfiler(f1,temp->fils[2]);
        }else{
          tab[i]=temp->cles[0];
          i++;
          tab[i]=temp->cles[1];
          i++;
          enfiler(f1,temp->fils[0]);
          enfiler(f1,temp->fils[1]);
          enfiler(f1,temp->fils[2]);
          if (temp->t==4){
            tab[i]=temp->cles[2];
            i++;
            enfiler(f1,temp->fils[3]);          
          }
        }
      }
    }
  }

  return ;
  
}

void replace_tab(int* tabcle,int nbcle,int i){
  for(int k=i;k<nbcle-1;k++){
    tabcle[k]=tabcle[k+1];
  }
}


int main (int argc, char **argv)
{
  Arbre234 a ;
  if (argc != 2)
    {
      fprintf (stderr, "il manque le parametre nom de fichier\n") ;
      exit (-1) ;
    }

  a = lire_arbre (argv [1]) ;

  printf ("==== Afficher arbre ====\n") ;
  
  afficher_arbre (a, 0) ;
  printf("le nombre de noeud est:%d\n",NombreCles(a));
  printf("la clé max est:%d\n",CleMax(a));
  printf("la clé min est:%d\n",CleMin(a));

  printf("la racine correspond au noeud rechercher ici 10\n");
  afficher_arbre(RechercherCle(a,10),0);
  printf("\n");
  printf("\n");
  printf("\n");
  printf("la racine correspond au noeud où la somme des clés est maximal\n");
  afficher_arbre(noeud_max(a),0);
  printf("\n");
  printf("\n");
  printf("\n");
  int f=0,n2=0,n3=0,n4=0;
  int* feuilles = &f;
  int* noeud2 = &n2;
  int* noeud3 = &n3;
  int* noeud4 = &n4;
  AnalyseStructureArbre (a,feuilles,noeud2,noeud3,noeud4);
  printf("la composition de l'arbre est: \n - feuille :%d \n - noeud2 :%d\n - noeud3 :%d\n - noeud4:%d\n",*feuilles,*noeud2,*noeud3,*noeud4);
    printf("affichage des clés en largeur\n");
  Afficher_Cles_Largeur(a);
  printf("\n");
  printf("\n");
  printf("\n");
  printf("affichage des clés triees\n");
  Affichage_Cles_Triees_Recursive(a);
  printf("\n");
  printf("\n");
  printf("\n");
  Affichage_Cles_Triees_NonRecursive(a);
  int nbcle=NombreCles(a);
  int tabcle[nbcle];
  int N=6542;
  srand(N);
  affecter_tab_Cles_Largeur(a,tabcle);


  printf("\n");
  printf("\n");
  printf("\n");
  printf("destruction de toute les clés de manière aléatoire\n");
  while(nbcle>0){
    int i=rand()%nbcle;
    nbcle;
    printf("\n");
    printf("\n");
    printf("\n");
    printf("suppression de la clé %d\n",tabcle[i]);
    Detruire_Cle(&a,tabcle[i]);
    afficher_arbre(a,0);
    replace_tab(tabcle,nbcle,i);
    nbcle--;
  }
  stop();
  afficher_arbre(a,0);
  printf("%d %d",MAX_FILE_SIZE,MAX_PILE_SIZE);
}
